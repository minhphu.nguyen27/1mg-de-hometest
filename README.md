# 1MG Data Engineer Home test
## Environment setup
Run Docker:
```
docker-compose up -d
```
Open address `localhost:8888` in the browser to view the notebook.

Environment requirement:
- PySpark 3
- Jupyter notebook

## Submission
Files:
- Jupyter notebook file: `notebook/process_data.ipynb`
- Input data: `data/input/green_tripdata_2013-09.csv`
- Output data: `data/output/tripdata`

Output data is saved in parquet format, with additional columns:
- `duration`: Duration in seconds of the trip
- `pickup_hour_of_day_onehot`: hour of day of `lpep_pickup_datetime`, it is one-hot encoded sparse vector
- `dropoff_hour_of_day_onehot`: hour of day of `Lpep_dropoff_datetime`, it is one-hot encoded sparse vector
- `pickup_day_of_week_onehot`: day of week of `lpep_pickup_datetime`, it is one-hot encoded sparse vector
- `dropoff_day_of_week_onehot`: day of week of `Lpep_dropoff_datetime`, it is one-hot encoded sparse vector
- `in_jfk_location`: equals 1 if the pickup or dropoff locations were at JFK airport, otherwise 0

> Note that Parquet doesn't provide native support Vector data type. Instead, Spark represents Vectors using StructType.
